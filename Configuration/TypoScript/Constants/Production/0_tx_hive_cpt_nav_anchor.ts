
plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor {
    view {
        # cat=plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_nav_anchor/Resources/Private/Templates/
        # cat=plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_nav_anchor/Resources/Private/Partials/
        # cat=plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_nav_anchor/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptnavanchor_hivecptnavanchornavigationrendernavanchor//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

#################################
## NAVIGATION ANCHOR CONSTANTS ##
#################################

plugin.tx_hive_cpt_nav_anchor {
    settings {
        lib {
            navigation {
                bShowStart = 0
            }
        }

        production {
            includePath {
                public = EXT:hive_cpt_nav_anchor/Resources/Public/
                private = EXT:hive_cpt_nav_anchor/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_cpt_nav_anchor/Resources/Public/
                }
            }
        }
    }
}