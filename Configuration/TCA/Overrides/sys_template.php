<?php
defined('TYPO3_MODE') or die();

$extKey = 'hive_cpt_nav_anchor';
$title = 'hive_cpt_nav_anchor';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptnavanchor_domain_model_navigation', 'EXT:hive_cpt_nav_anchor/Resources/Private/Language/locallang_csh_tx_hivecptnavanchor_domain_model_navigation.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptnavanchor_domain_model_navigation');