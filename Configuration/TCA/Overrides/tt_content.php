<?php
defined('TYPO3_MODE') or die();

/***************
 * Plugin
 ***************/
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HIVE.HiveCptNavAnchor',
    'Hivecptnavanchornavigationrendernavanchor',
    'hive_cpt_nav_anchor :: Navigation :: renderNavAnchor'
);

$extKey = strtolower('hive_cpt_nav_anchor');

$pluginName = strtolower('Hivecptnavanchornavigationrendernavanchor');
$pluginSignature = str_replace('_', '', $extKey) . '_' . str_replace('_', '', $pluginName);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'. $extKey . '/Configuration/FlexForms/Config.xml');