################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            SectionAnchor2575WithOffset {
                title = hive Backend Layout :: Section A/B/C with Offset:: 25% 75%
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [0] SECTION A/B/C with Offset :: 25%
                                        colPos = 0
                                        colspan = 3
                                    }
                                    2 {
                                        name = [1] SECTION A/B/C with Offset :: 75%
                                        colPos = 1
                                        colspan = 9
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchor2575WithOffset.gif
            }
        }
    }
}