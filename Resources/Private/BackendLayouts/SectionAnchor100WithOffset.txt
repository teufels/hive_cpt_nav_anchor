################################
#### BACKENDLAYOUT: DEFAULT ####
################################
mod {
    web_layout {
        BackendLayouts {
            SectionAnchor100WithOffset {
                title = hive Backend Layout :: SectionA/B/C with Offset :: 100%
                config {
                    backend_layout {
                        colCount = 12
                        rowCount = 1
                        rows {
                            1 {
                                columns {
                                    1 {
                                        name = [0] SECTION A/B/C :: NOT VISIBLE IN FRONTEND
                                        colPos = 0
                                        colspan = 12
                                    }
                                }
                            }
                        }
                    }
                }
                icon = EXT:hive_cpt_nav_anchor/Resources/Public/Icons/BackendLayouts/SectionAnchorWithOffset.gif
            }
        }
    }
}